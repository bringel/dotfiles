;;; Change the load-path string below to point to the .emacs.d
;;; directory on your machine.  For example, Will uses the string
;;; "~/.emacs.d" on his laptop running Mac OS X.  Windoze users
;;; will probably want to put the .emacs.d directory in the
;;; "My Documents" directory.
(add-to-list 'load-path "~/.emacs.d")
(add-to-list 'load-path "~/.emacs.d/evil")

;; ;;;;;;;;;;;;;;;;;;;;;;
;; ;;      Config      ;;
;; ;;;;;;;;;;;;;;;;;;;;;;
(setq x-select-enable-clipboard t)
;;; For Mac users who want to use the option key as Meta (instead of
;;; the Apple key).
;; (setq mac-command-key-is-meta nil)
;; ;;; For Mac users only!
;;                                         ; A nice font for code.
;;                                         ; (set-default-font "*apple-monaco-*14*") ;;; monaco 14 is okay for programming
;; ;;; Hilight the selected region
(setq transient-mark-mode t)
;;; Avoid iconizing emacs.
;;(global-unset-key "\C-z")
;;(global-unset-key "\C-s")
;;(global-unset-key "\C-c")
;;; Jump to a specific line of the current buffer
;;(glogal-set-key "M-g" 'goto-line)
(global-set-key "\M-g" 'goto-line)
(global-set-key "\C-r" 'kill-region)

;;(global-set-key (kbd "C-c") 'comment-or-uncomment-region)
(define-key global-map (kbd "RET") 'newline-and-indent)
;; ;;thrash emacs mode conversion in evil
;; (define-key global-map (kbd "\C-z") 'suspend-emacs)

;; ;;show the time
;; (display-time)
;; ;;turn menu bar off
;; (menu-bar-mode -1)
;; ;;; Use spaces instead of tabs for indentation.
(setq indent-tabs-mode nil)

;;; Turn on syntax-highlighting.
(global-font-lock-mode t)
(load-library "font-lock")
(setq font-lock-maximum-decoration t)

;; ;;;;;;;;;;;;;;;;;;;;;;
;; ;;      Scheme      ;;
;; ;;;;;;;;;;;;;;;;;;;;;;

;;; Load the Quack mode, which is an advanced Scheme mode.
(require 'quack)

;;; Loading/creating a file/buffer whose name ends with '.ss'
;;; automagically switches Emacs to Scheme mode.
(setq auto-mode-alist (cons '("\\.ss" . scheme-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.pc" . scheme-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.scm" . scheme-mode) auto-mode-alist))
;;; Change the program name string below to point to Petite Chez
;;; Scheme on your machine.  For example, Will uses the string
;;; "/usr/bin/petite" on his laptop running Mac OS X.
(setq scheme-program-name "petite")

;;; Teach Emacs how to properly indent
;;; certain Scheme special forms
;;; (such as 'pmatch')
(put 'cond 'scheme-indent-function 0)
(put 'for-each 'scheme-indent-function 0)
(put 'pmatch 'scheme-indent-function 1)
(put 'match 'scheme-indent-function 1)
(put 'match+ 'scheme-indent-function 1)
(put 'union-case 'scheme-indent-function 2)
(put 'cases 'scheme-indent-function 1)
(put 'let-values 'scheme-indent-function 1)
(put 'call-with-values 'scheme-indent-function 2)
(put 'syntax-case 'scheme-indent-function 2)
(put 'test 'scheme-indent-function 1)
(put 'test-check 'scheme-indent-function 1)
(put 'test-divergence 'scheme-indent-function 1)
(put 'make-engine 'scheme-indent-function 0)
(put 'with-mutex 'scheme-indent-function 1)
(put 'trace-lambda 'scheme-indent-function 1)
(put 'timed-lambda 'scheme-indent-function 1)
(put 'tlambda 'scheme-indent-function 1)
(put 'library 'scheme-indent-function 0)

;;; minikanren-specific indentation
(put 'lambdaf@ 'scheme-indent-function 1)
(put 'lambdag@ 'scheme-indent-function 1)
(put 'fresh 'scheme-indent-function 1)
(put 'exists 'scheme-indent-function 1)
(put 'exist 'scheme-indent-function 1)
(put 'nom 'scheme-indent-function 1)
(put 'run 'scheme-indent-function 2)
(put 'conde 'scheme-indent-function 0)
(put 'conda 'scheme-indent-function 0)
(put 'condu 'scheme-indent-function 0)
(put 'project 'scheme-indent-function 1)
(put 'run* 'scheme-indent-function 1)
(put 'run1 'scheme-indent-function 1)
(put 'run2 'scheme-indent-function 1)
(put 'run3 'scheme-indent-function 1)
(put 'run4 'scheme-indent-function 1)
(put 'run5 'scheme-indent-function 1)
(put 'run6 'scheme-indent-function 1)
(put 'run7 'scheme-indent-function 1)
(put 'run8 'scheme-indent-function 1)
(put 'run9 'scheme-indent-function 1)
(put 'run10 'scheme-indent-function 1)
(put 'run11 'scheme-indent-function 1)
(put 'run12 'scheme-indent-function 1)
(put 'run13 'scheme-indent-function 1)
(put 'run15 'scheme-indent-function 1)
(put 'run22 'scheme-indent-function 1)
(put 'run34 'scheme-indent-function 1)

;;;;;;;;;;;;;;;;;;;;;;
;;       COLORS     ;;
;;;;;;;;;;;;;;;;;;;;;;

;;; For folks who like pretty colors on their screens:
;;we can manually set the color theme here, but it bugs out
;;since we are using the extra color theme goodies, so it is best
;;to set the color theme using alt x color-theme-select
                                        ;(require 'color-theme)
;;add this to use color theme
;; (add-to-list 'load-path "~/.emacs.d/color-theme.el")
;; (require 'color-theme)
;; (eval-after-load "color-theme"
;;   '(progn
;;      (color-theme-initialize)
;;      (color-theme-taming-mr-arneson)))

;;(color-theme-taming-mr-arneson)

(if (fboundp 'global-font-lock-mode)
    (global-font-lock-mode 1); Emacs
  (setq font-lock-auto-fontify t)); XEmacs
;;(set-background-color "black")
;;(set-foreground-color "PaleTurquoise1")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;       PAREN BALANCING     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is balanced.el and should not be run at the same time with paredit

;;                                         (autoload 'balanced-on "balanced" "Turn on balanced ``mode''" t)
;;                                         (add-hook 'scheme-mode-hook 'balanced-on)
;;                                         (add-hook 'inferior-scheme-mode-hook 'balanced-on)

(autoload 'paredit-mode "paredit"
  "Minor mode for pseudo-structurally editing Lisp code." t)
(add-hook 'emacs-lisp-mode-hook       (lambda () (paredit-mode +1)))
(add-hook 'lisp-mode-hook             (lambda () (paredit-mode +1)))
(add-hook 'lisp-interaction-mode-hook (lambda () (paredit-mode +1)))
(add-hook 'scheme-mode-hook           (lambda () (paredit-mode +1)))
(add-hook 'c-mode-hook                (lambda () (paredit-mode +1)))
(add-hook 'haskell-mode-hook                (lambda () (paredit-mode +1)))
(add-hook 'c++-mode-hook                (lambda () (paredit-mode +1)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;      custom variables     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(display-time-mode t)
 '(inhibit-startup-screen t)
 '(paren-blinking "t" t)
 '(paren-matching t t)
 '(quack-default-program "petite")
 '(quack-fontify-style (quote plt))
 '(quack-newline-behavior (quote newline))
 '(quack-pltish-fontify-keywords-p t)
 '(quack-pltish-keywords-to-fontify (quote ("->" "=>" "and" "begin" "begin0" "c-declare" "c-lambda" "call-with-current-continuation" "call-with-input-file" "call-with-output-file" "call/cc" "case" "case-lambda" "class" "class*" "class*/names" "class100" "class100*" "compound-unit/sig" "cond" "cond-expand" "define" "define-macro" "define-module" "define-public" "define-signature" "define-struct" "define-syntax" "define-syntax-set" "define-values" "define-values/invoke-unit/sig" "define/contract" "define/override" "define/private" "define/public" "delay" "do" "else" "exit-handler" "field" "if" "import" "inherit" "inherit-field" "init" "init-field" "init-rest" "instantiate" "interface" "lambda" "let" "let*" "let*-values" "let+" "let-syntax" "let-values" "let/ec" "letrec" "letrec-values" "letrec-syntax" "match" "match+" "match-lambda" "match-lambda*" "match-let" "match-let*" "match-letrec" "match-define" "mixin" "module" "opt-lambda" "or" "override" "override*" "namespace-variable-bind/invoke-unit/sig" "parameterize" "private" "private*" "protect" "provide" "provide-signature-elements" "provide/contract" "public" "public*" "quote" "receive" "rename" "require" "require-for-syntax" "send" "send*" "set!" "set!-values" "signature->symbols" "super-instantiate" "syntax-case" "syntax-case*" "syntax-error" "syntax-rules" "unit/sig" "unless" "when" "with-handlers" "with-method" "with-syntax")))
 '(quack-pretty-lambda-p t)
 '(quack-programs (quote ("petite" "bigloo" "csi" "gosh" "gsi" "guile" "kawa" "mit-scheme" "mred -z" "mzscheme" "mzscheme -M errortrace" "rs" "scheme" "scheme48" "scsh" "sisc" "stklos" "sxi")))
 '(quack-remap-find-file-bindings-p nil)
 '(quack-run-scheme-always-prompts-p nil)
 '(quack-switch-to-scheme-method (quote cmuscheme))
 '(show-paren-mode t)
 '(split-height-threshold 80)
 '(split-width-threshold 80))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "unknown" :family "DejaVu LGC Sans Mono"))))
 '(quack-pltish-comment-face ((((class color) (background dark)) (:foreground "orange2" :slant italic))))
 '(quack-pltish-defn-face ((t (:foreground "yellow2" :weight bold))))
 '(quack-pltish-keyword-face ((t (:foreground "cyan2"))))
 '(quack-pltish-paren-face ((((class color) (background dark)) (:foreground "cyan3"))))
 '(quack-pltish-selfeval-face ((((class color) (background dark)) (:foreground "green"))))
 '(quack-threesemi-semi-face ((((class color) (background dark)) (:background "white" :foreground "white" :box nil))))
 '(quack-threesemi-text-face ((((class color) (background dark)) (:background "cyan4" :foreground "white" :box nil)))))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;       auto complete       ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path "~/.emacs.d/")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d//ac-dict")
(ac-config-default)

(require 'auto-complete)
(global-auto-complete-mode t)
(setq ac-auto-start 2)
(setq ac-dwim t)

                                        ;(add-to-list 'ac-dictionary-directories "~/.emacs.d/dict")
                                        ;(require 'auto-complete-config)
                                        ;(ac-config-default)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;          Smex            ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;       line numbers        ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;better line numbers mode.
;;(require 'line-num)
(require 'linum+)
;;spacing for line-num line numbers
(setq linum-format "%d ")
;;turn on line numbers automatically
(global-linum-mode 1)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;     backup/autosave     ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar backup-dir (expand-file-name "~/.emacs.d/backup/"))
(defvar autosave-dir (expand-file-name "~/.emacs.d/autosave/"))
(setq backup-directory-alist (list (cons ".*" backup-dir)))
(setq auto-save-list-file-prefix autosave-dir)
(setq auto-save-file-name-transforms `((".*" ,autosave-dir t)))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;  functions and macros    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Show line of matching paren in minibuffer
(defadvice show-paren-function
  (after show-matching-paren-offscreen activate)
  "If the matching paren is offscreen, show the matching line in the
echo area. Has no effect if the cacter before point is not of
the syntax class ')'."
  (interactive)
  (if (not (minibuffer-prompt))
      (let ((matching-text nil))
        ;; Only call `blink-matching-open' if the character before point
        ;; is a close parentheses type character. Otherwise, there's not
        ;; really any point, and `blink-matching-open' would just echo
        ;; "Mismatched parentheses", which gets really annoying.
        (if (char-equal (char-syntax (char-before (point))) ?\))
            (setq matching-text (blink-matching-open)))
        (if (not (null matching-text))
            (message matching-text)))))

(defun iwb ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

(defun unwrap ()
  "Hopefully this works as a unwrap command"
  (interactive)
  (point-to-register 'y)
  (while (not (or (looking-at "(") (looking-at "\\[") (looking-at "{"))) (forward-char 1))
  (forward-char 1)
  (if mark-active
      (progn (exchange-point-and-mark)
             (point-to-register 'z)
             (exchange-point-and-mark))
    (set-register 'z 'nil))
  (set-mark-command 'nil)
  (backward-char 1)
  (forward-list 'nil)
  (backward-char 1)
  (kill-region (mark) (point))
  (balanced-delete-char 'nil)
  (yank)
  (if (get-register 'z)
      (progn
        (register-to-point 'z)
        (set-mark-command 'nil)))
  (register-to-point 'y))

(column-number-mode 1)

(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'er 'eval-region)

;; ;;copy the line above
;; (fset 'la
;;       (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("      " 0 "%d")) arg)))

;; (require 'key-chord) ;;map simultaneous key presses
;; (key-chord-mode 1)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;   paredit awesomeness   ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;WARNING: this makes no sense if you don't use dvorak

;; ;; (key-chord-define-global "@@" 'unwrap)
;; ;; (key-chord-define-global "&&"  'paredit-splice-sexp)
;; ;; (key-chord-define-global "^^"  'paredit-split-sexp)
;; ;; (key-chord-define-global "**"  'paredit-join-sexps)
;; ;; (key-chord-define-global ">>" 'paredit-forward-slurp-sexp)
;; ;; (key-chord-define-global "<<" 'paredit-backward-slurp-sexp)
;; ;; (key-chord-define-global "%%" 'paredit-forward-barf-sexp)
;; ;; (key-chord-define-global "##" 'paredit-backward-barf-sexp)

;;;;;;;;;;;;;;;;;;;;;;;;;
;;;     evil mode     ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'evil)
(evil-mode 1)
;;(setq evil-auto-indent t)

(defun nmap (key def)
  (define-key evil-normal-state-map key def))

(defun mmap (key def)
  (define-key evil-motion-state-map key def))

(defun rmap (key def)
  (define-key evil-replace-state-map key def))

(defun imap (key def)
  (define-key evil-insert-state-map key def))

(defun vmap (key def)
  (define-key evil-visual-state-map key def))

(defun my-save-all-and-close ()
  (interactive)
  (save-buffers-kill-terminal 'force))

(defun clear-trailing-whitespace-wrapper ()
  (interactive)
  ;;(delete-horizontal-space)
  ;;(delete-trailing-whitespace)
  (evil-normal-state)
  (save-excursion
    (replace-regexp "^ +$" "" nil (buffer-end -1) (buffer-end 1))))

(evil-ex-define-cmd "[x]it" "wq")
(evil-ex-define-cmd "[xa]ll" "wqall")
(evil-ex-define-cmd "chk" 'hs-lint)
;;(evil-ex-define-cmd "[wqa]ll" 'save-buffers-kill-terminal)
(evil-ex-define-cmd "[wqa]ll" 'my-save-all-and-close)
(imap [escape] 'clear-trailing-whitespace-wrapper)
(rmap [escape] 'clear-trailing-whitespace-wrapper)

;;(provide 'evil+new-maps)


;; ;;(key-chord-define-global ".c"  'evil-normal-state) ; super ESC
;; ;;(key-chord-define-global "C>"  'evil-emacs-state)
;; ;;this way we can do thinks like swap sexps in emacs
;; ;;(key-chord-define-global "!!" 'ESC-prefix)
;; ;;switch windows with one combo
;; ;;(key-chord-define-global "zv" 'evil-window-prev) ; df twice
;; ;;(key-chord-define-global "kj" 'evil-window-next) ; df twiceu

;; ;;;remap ctrl-c to esc
;; (define-key evil-esc-map "\C-c" 'evil-esc)

;; ;;custom evil mappings for dvorak
;; ;; (nmap "h" 'evil-backward-char)
;; ;; (nmap "s" 'evil-forward-char)
;; ;; (vmap "n" 'evil-previous-line)
;; ;; (nmap "n" 'evil-previous-line)
;; ;; (nmap "t" 'evil-next-line)
;; ;; (vmap "t" 'evil-next-line)
;; ;; (nmap "U" 'evil-change)
;; ;; (nmap "j" 'evil-change-line)
;; ;; (nmap "J" 'evil-join)
;; ;; (nmap "r" 'evil-replace)
;; ;; (nmap "R" 'evil-change-whole-line)
;; ;; (mmap "_" 'evil-beginning-of-line)
;; ;; (mmap "-" 'evil-end-of-line)
;; ;; (mmap "B" 'evil-backward-word-begin)
;; ;; (mmap "l" 'evil-search-next)
;; ;; (mmap "L" 'evil-search-previous)
;; ;;(mmap "c" 'evil-backward-char)
;; ;;(mmap "g" 'evil-window-top)
;; ;;(mmap "G" 'evil-window-bottom)
;; ;;(nmap "L" 'evil-replace-state)
;; ;;(nmap "r" 'evil-substitute)
;; ;;(mmap "J" 'evil-find-char-to-backward)

(define-key evil-normal-state-map "\C-y" 'yank)
(define-key evil-normal-state-map "\C-c\C-r" 'comment-or-uncomment-region)
(define-key evil-normal-state-map "\C-r" 'kill-region)
(define-key evil-normal-state-map "\M-g" 'goto-line)
(define-key evil-normal-state-map "\C-c\C-z" 'suspend-emacs)

(define-key evil-insert-state-map "\C-y" 'yank)
(define-key evil-insert-state-map "\C-c\C-r" 'comment-or-uncomment-region)
(define-key evil-insert-state-map "\C-r" 'kill-region)
(define-key evil-insert-state-map "\M-g" 'goto-line)
(define-key evil-insert-state-map "\C-k" 'kill-line)
(define-key evil-insert-state-map (kbd "RET") 'newline-and-indent)
(define-key evil-normal-state-map "\C-c\C-z" 'suspend-emacs)
;; ;;Evil leader
;; ;;I don't really use it. But its there incase I ever want to
;; (require 'evil-leader)
;; ;;(evil-leader/set-key "." 'p )

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;    haskell mode    ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; Haskell
;; (setq auto-mode-alist
;;       (append '(("\\.hs$" . haskell-mode)
;;                 ("\\.hsc$" . haskell-mode)
;;                 ("\\.chs$" . haskell-mode)
;;                 ("\\.bs$" . haskell-mode)
;;                 ("\\.cabal$" . haskell-mode)
;;                 ("\\.ly$" . literate-haskell-mode)
;;                 ("\\.x$"  . haskell-mode)
;;                 ("\\.y$"  . haskell-mode)
;;                 ("\\.y.pp$"  . haskell-mode)
;;                 ("\\.bench$" . haskell-mode)
;;                 )
;;               auto-mode-alist))
;; (autoload 'haskell-mode "haskell-mode" "Major mode for editing Haskell code" t)

;; (load "~/.emacs.d/haskell-site-file")

;; (add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
;; (add-hook 'haskell-mode-hook 'font-lock-mode)

;; ;;enable only one at a time
;; ;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;; ;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;; ;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;      flymake       ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;

;; (defun flymake-haskell-init ()
;;   "When flymake triggers, generates a tempfile containing the
;;   contents of the current buffer, runs `hslint` on it, and
;;   deletes file. Put this file path (and run `chmod a+x hslint`)
;;   to enable hslint: https://gist.github.com/1241073"
;;   (let* ((temp-file   (flymake-init-create-temp-buffer-copy
;;                        'flymake-create-temp-inplace))
;;          (local-file  (file-relative-name
;;                        temp-file
;;                        (file-name-directory buffer-file-name))))
;;     (list "hslint" (list local-file))))

;; (defun flymake-haskell-enable ()
;;   "Enables flymake-mode for haskell, and sets <C-c d> as command
;;   to show current error."
;;   (when (and buffer-file-name
;;              (file-writable-p
;;               (file-name-directory buffer-file-name))
;;              (file-writable-p buffer-file-name))
;;     (local-set-key (kbd "C-c d") 'flymake-display-err-menu-for-current-line)
;;     (flymake-mode t)))

;; ;; Forces flymake to underline bad lines, instead of fully
;; ;; highlighting them; remove this if you prefer full highlighting.
;; ;;(custom-set-faces
;; ;; '(flymake-errline ((((class color)) (:underline "red"))))
;; ;; '(flymake-warnline ((((class color)) (:underline "yellow")))))

;; ;; Forces flymake to underline bad lines, instead of fully
;; ;; highlighting them; remove this if you prefer full highlighting.
;; ;; (custom-set-faces
;; ;;'(flymake-errline ((((class color)) (:underline "red"))))
;; ;;'(flymake-warnline ((((class color)) (:underline "yellow")))))

;; (require 'hs-lint)    ;; https://gist.github.com/1241059
;; (require 'haskell-ac) ;; https://gist.github.com/1241063

;; (defun my-haskell-mode-hook ()
;;   "hs-lint binding, plus autocompletion and paredit."
;;   (local-set-key "\C-cl" 'hs-lint)
;;   (setq ac-sources
;;         (append '(ac-source-yasnippet
;;                   ac-source-abbrev
;;                   ac-source-words-in-buffer
;;                   my/ac-source-haskell)
;;                 ac-sources))
;;   (dolist (x '(haskell literate-haskell))
;;     (add-hook
;;      (intern (concat (symbol-name x)
;;                      "-mode-hook"))
;;      'turn-on-paredit)))

;; (eval-after-load 'haskell-mode
;;   '(progn
;;      (require 'flymake)
;;      (push '("\\.l?hs\\'" flymake-haskell-init) flymake-allowed-file-name-masks)
;;      (add-hook 'haskell-mode-hook 'flymake-haskell-enable)
;;      (add-hook 'haskell-mode-hook 'my-haskell-mode-hook)))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;      go mode       ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (require 'go-autocomplete)
;; (require 'auto-complete-config)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;      auto indent   ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; (require 'auto-indent-mode)
;; ;; (auto-indent-global-mode)
;; ;;(add-hook 'emacs-lisp-mode-hook 'auto-indent-minor-mode)
;; ;;(add-hook 'emacs-scheme-mode-hook 'auto-indent-minor-mode)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;      undo tree     ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'undo-tree)
(global-undo-tree-mode)
