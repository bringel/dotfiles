;; schemefont.el

;; I add fontlocking to the scheme mode hook _only_ if we're
;; running xwindows.  If I'm running a tty, the variable
;; window-system will be set to nil.

;; The font-lock-defaults portion of the following command sets
;; up font-lock to fontify according to the rules of the variable
;; scheme-font-lock-keywords.  It also changes the syntax table
;; for scheme-mode so that the `-', `*' and `:' characters become
;; normal word characters.

(if window-system
    (progn
      (add-hook 'scheme-mode-hook 'turn-on-font-lock)
      (add-hook 'scheme-mode-hook
	     (function 
	       (lambda () 
		 (setq font-lock-defaults
		       '(scheme-font-lock-keywords  ; keywords defn
			 nil			; don't do ``keywords only''
			 nil			; not case sensitive
			 ((?\- . "w") (?\* . "w") (?\: . "w"))
					; intraword syntax.
			 )))))
      ))

;; I'm annoyed by colors when I edit, so I use styles for
;; strings, comments and keywords instead.  If you like colors,
;; don't add this hook.

(add-hook 'font-lock-mode-hook
	  (function
	   (lambda ()
	     (setq font-lock-string-face 'underline)
	     (setq font-lock-comment-face 'italic)
	     (setq font-lock-keyword-face 'bold))))

;; cryptic documentation on the format of this list can be found
;; in the documentation for ``font-lock-keywords''.  This
;; definition defines the syntax (by regexps) which should be
;; fontified.

(defvar scheme-font-lock-keywords
  (append
   (mapcar (lambda (x) (format "(%s\\b" x))
	   '(quote begin unless when
		   if case cond and or
		   define set! lambda rec
		   fluid-let
		   synlambda keywords extend-syntax))
   '("[[(]else\\b" 
     "(let\\(rec\\|[*]\\|\\)\\b"
     "(trace-\\(lambda\\|let\\)\\b"
     ("'()" 0 font-lock-string-face keep)
     ("#\\([tf]\\|\\\\.\\)" 0 font-lock-string-face keep)
     ("'[-$%><_=?:*]*\\w+\\([-_<>=?:*$%.]+\\w*\\)*" 
	0 font-lock-string-face keep)	
     "("
     ")"
     "[][`',@]"
     ))

  "*The keywords to highlight in scheme.  
More information can be found in the documentation for font-lock-keywords")


