(library
  (compiler frontend convert-complex-datum)
  (export convert-complex-datum)
  (import
   (chezscheme)
   (framework wrappers)
   (framework helpers)
   (framework match)
   (compiler backend libhelp)
   (compiler scheme-debug)
   )
  
  #|
  
  INPUT: 
  
  ProgramExpr
  Expruvar
  |(quote datum)
  |(if Expr Expr Expr)
  |(begin Expr* Expr)
  |(lambda (uvar*) Expr)
  |(let ([uvar Expr]*) Expr)
  |(letrec ([uvar Expr]*) Expr)
  |(set! uvar Expr)
  |(prim Expr*)
  |(Expr Expr*)
  
  OUTPUT:
  
  ProgramExpr
  Expruvar
  |(quote Immediate)
  |(if Expr Expr Expr)
  |(begin Expr* Expr)
  |Lambda
  |(let ([uvar Expr]*) Expr)
  |(letrec ([uvar Lambda]*) Expr)
  |(set! uvar Expr)
  |(prim Expr*)
  |(Expr Expr*)
  Lambda
  (lambda (uvar*) Expr)
  immediate | () | #t | #f
  
  |#
  
  (define-who convert-complex-datum
    
    (define handle-complex-datum
      (lambda (datum)
        (define handle-list
          (lambda (ls)
            ((dbg 5) "in handle list ~s\n" ls)
            (cond
              [(atom? ls) `(quote ,ls)]
              [else `(cons ,(if (immediate? (car ls)) `(quote ,(car ls)) (car ls)) ,(handle-list (cdr ls)))])))
        
        (define handle-vector
          (lambda (ptr v n)
            ((dbg 5) "in handle-vector ~s\n" v)
            (cond
              [(null? v) (values '() n) ]
              [(immediate? v)  (values `((vector-set! ,ptr (quote ,n) (quote ,v))) n)]
              [(immediate? (car v))
               (let-values ([(a n^) (handle-vector ptr (car v) n)]
                            [(d _^) (handle-vector ptr  (cdr v) (add1 n))])
                 (values `(,@a ,@d) _^))]
              [else
               (let-values ([(d _^) (handle-vector ptr  (cdr v) (add1 n))])
                 (values `((vector-set! ,ptr (quote ,n)  ,(if (immediate? (car v)) `(quote ,(car v)) (car v))) ,@d) _^)) ])))
        
        ((dbg 5) "datum ~s\n" datum)
        (match datum
          [(,prim ,x) (guard (prim? prim)) `(,prim ,x)]
          [(quote ,[x]) x]
          [(,[d*] ...)
           ((dbg 5) "d* ~s\n" d*)
           (handle-list d*)]
          [(,[a] . ,[d]) (handle-list `(,a . ,d))]
          [#(,[d*] ...)
           ((dbg 5) who "about to handle vector ~s\n" d*)
           (let*-values ([(tmp) (unique-name 'tmp)]
                         [(t) (unique-name 't)]
                         [(b l) (handle-vector tmp d* 0)])
             `(let ([,tmp (make-vector (quote ,l))])
                ,(make-begin `(,b ...  ,tmp))))]
          [,x (guard (or (immediate? x) (int64? x))) x]
          [,err (error who "unhandled complex datum ~s" err)])))

    (define complex?
      (lambda (datum)
        (match datum
          [(quote (,d* ...)) #t]
          [(quote #(,d* ...)) #t]
          [,_ #f])))

    (define convert-bindings
      (lambda (bindings)
        ((dbg 5) who "bindings ~s\n" bindings)
        (match bindings
          [(,u (lambda (,u* ...) ,body))
           (let*-values ([(tmps e^) (convert-literals body)]
                         [(be e) (Expr e^)])
             (values `(,tmps ... ,be ...) `(,u (lambda ,u* ,e))))]
          [(,u ,expr) (guard (or (label? u) (uvar? u)))
           (let*-values ([(tmps e^) (convert-literals expr)]
                         [(be e) (Expr e^)])
             (values `(,tmps ... ,be ...) `(,u ,e)))]
          [(,[bb b] ,[bb* b*] ...)
           (values `(,bb ... ,bb* ... ...) `(,b ,b* ...))]
          [() (values '() '())]
          [,err (error who "invalid binding form ~s\n" err)])))

    (define convert-literals
      (lambda (b)
        ((dbg 5) "bindings ~s\n" b)
        (cond
          [(null? b)
           ((dbg 5) "null ~s\nn" b)
           (values '() '())]
          [(atom? b)
           ((dbg 5) "atom ~s\n" b)
           (values '() b)]
          [(pair? (car b))
           ((dbg 5) "pair ~s\n" b)
           (let-values ([(a b) (convert-literals (car b))]
                        [(c d) (convert-literals (cdr b))])
             (values `(,@a ,@c) (cons b d)))]
          [(and (eq? (car b) 'quote) (immediate? (cadr b)))  ((dbg 5) "HERE\n") (values '() b)]
          [(match b
             [(quote ,i) #t]
             [,_ #f])
           ((dbg 5) "match ~s\n" b)
           (let-values ([(tmp) (unique-name 'tmp)]
                        [(ba a) (Expr b)])
             (values `(,@ba (,tmp ,a)) tmp))]
          [else
           ((dbg 5) "else ~s\n" b)
           (let-values ([(bd d) (convert-literals (cdr b))])
             (values bd (cons (car b) d)))])))

    (define Expr
      (lambda (program)
        (match program
          [(false) (values '() '(false))]
          [(true)  (values '() '(true))]
          [(quote ,i) (guard (immediate? i)) (values '() `(quote ,i))]
          [(quote ,datum) (values '() (handle-complex-datum `(quote ,datum)))]
          [(if ,[bp p] ,[bc c] ,[ba a]) (values `(,bp ... ,bc ... ,ba ...)`(if ,p ,c ,a))]
          [(begin ,[be* e*] ... ,[bp p]) (values `(,be* ... ... ,bp ...) (make-begin `(,e* ... ,p)))]
          [(let (,bindings* ...) ,[bexpr expr])
           (let-values ([(b e) (convert-bindings bindings*)])
             (values `(,b ... ,bexpr ...) `(let ,e ,expr)))]
          [(letrec (,bindings* ...) ,[bexpr expr])
           (let-values ([(b e) (convert-bindings bindings*)])
             (values `(,b ...  ,bexpr ...) `(letrec ,e ,expr)))]
          [(lambda (,uvar* ...) ,[bexpr expr]) (values bexpr `(lambda ,uvar* ,expr))]
          [(set! ,uvar ,e)
           (let-values ([(bexpr expr) (Expr e)]
                        [(tmp) (unique-name 'tmp)])
             (if (complex? e)
                 (values `(,bexpr ... (,tmp ,expr)) `(set! ,uvar ,tmp))
                 (values bexpr `(set! ,uvar ,expr))))]
          [(,prim ,[bv* v*] ...) (guard (prim? prim)) (values `(,bv* ... ...) `(,prim ,v* ...))]
          [,x (guard (or (uvar? x) (label? x) (immediate? x) (int64? x))) (values '()  x)]
          [(,[bexpr expr] ,[bexpr* expr*] ...) (values  `(,bexpr ... ,bexpr* ... ...) `(,expr ,expr* ...))]
          [,err (error who "unmatched expression ~s\n" err)])))
    
    (lambda (program)
      (match program
        [,[Expr -> blocks prog]
         (if (andmap null? blocks)
             prog
             `(let ,(if (list? (car blocks)) blocks `(,blocks)) ,prog))]
        [,err (error who "failure in convert-complex-datum ~s\n" err)]))

    );end define who
  );end library
