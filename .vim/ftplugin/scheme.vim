runtime! ftplugin/lisp.vim ftplugin/lisp_*.vim ftplugin/lisp/*.vim

setl lispwords+=library,import,export,let-values,mlet-values*,letrec-values
setl lispwords+=trace-define,trace-lambda,define-who,match,match+
setl lispwords+=trace-match,trace-match+,pmatch,define-record-type,fields
setl lispwords+=define-union,define-syntax,syntax-case,syntax-rules
setl lispwords+=union-case,fresh
