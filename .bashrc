
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
PS1="[\u@\h \W]\$ "

alias spring13="cd ~/Documents/College/Spring\ 2013/"

export EDITOR=emacs

export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/texbin
export GEM_HOME=/usr/lib/ruby/gems

export CLICOLOR=true
#sets up the color scheme for list export
export LSCOLORS=gxfxcxdxbxegedabagacad
#enables color for iTerm
export TERM=xterm-color

### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

##fresh keeps all your dotfiles organized
source ~/.fresh/build/shell.sh

##We don't want Library to be hidden so let's do this every time I log in.
chflags nohidden ~/Library

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
