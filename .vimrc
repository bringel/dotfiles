set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'surround.vim'
Bundle 'Valloric/YouCompleteMe'
Bundle 'Raimondi/delimitMate'
Bundle 'scrooloose/syntastic'
Bundle 'scrooloose/nerdcommenter'
Bundle 'Lokaltog/powerline'
Bundle 'fugitive.vim'
Bundle 'kien/rainbow_parentheses.vim'

filetype plugin indent on

set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
let g:Powerline_symbols = 'fancy'
set laststatus=2

"Options for YouCompleteMe
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
let g:ycm_confirm_yc_extra_conf = 0

"Rainbow Parenthesis
au VimEnter * RainbowParenthesesToggleAll
"au Syntax * RainbowParenthesesLoadRound
"au Syntax * RainbowParenthesesLoadSquare
"au Syntax * RainbowParenthesesLoadBraces

"DelimitMatb options
let delimitMate_quotes = "\""

" Sets how many lines of history VIM has to remember
set history=700

"spaces instead of tabs
set tabstop=8
set shiftwidth=8
set expandtab

"set a dictionary file
autocmd BufRead *.scm set dictionary+=~/.vim/dictionary/scheme/csug 
autocmd FileType scheme syn keyword schemeSyntax +inf.0 -inf.0 
autocmd BufRead *.scm set dictionary+=~/.vim/dictionary/scheme/tspl 
autocmd BufRead *.ss set dictionary+=~/.vim/dictionary/scheme/csug 
autocmd BufRead *.ss set dictionary+=~/.vim/dictionary/scheme/tspl 

"commenting options
autocmd FileType scheme set commentstring=;;%s

autocmd BufRead *.pde set syntax=c 
autocmd BufRead *.cl set syntax=c
autocmd BufRead *.jsp set syntax=html
autocmd BufRead *.go set syntax=go
au BufRead,BufNewFile *.go set filetype=go
autocmd BufRead *.sls set syntax=scheme
" Enable filetype plugin
filetype plugin on
filetype indent on

autocmd BufRead *.md set ft=markdown

set smartindent
set autoindent
set cinoptions+=j1
let java_mark_braces_in_parens_as_errors=1
let java_highlight_all=1
let java_highlight_debug=1
let java_ignore_javadoc=1
let java_highlight_java_lang_ids=1
" Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
" let mapleader = ","
"let g:mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" Fast editing of the .vimrc
map <leader>e :e! ~/.vim_runtime/vimrc<cr>

" When vimrc is edited, reload it
autocmd! bufwritepost vimrc source ~/.vim_runtime/vimrc


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the curors - when moving vertical..
set so=7

set wildmenu "Turn on WiLd menu

set ruler "Always show current position

set cmdheight=2 "The commandbar height

set hid "Change buffer - without saving

" Set backspace config
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

set ignorecase "Ignore case when searching
set smartcase

set hlsearch "Highlight search things

set incsearch "Make search act like search in modern browsers
set nolazyredraw "Don't redraw while executing macros 

set magic "Set magic on, for regular expressions

set showmatch "Show matching bracets when text indicator is over them
set mat=2 "How many tenths of a second to blink

" No sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500
set sw=2 "no of spaces for indenting
set ts=2 "show \t as 2 spaces
set number

 " easy access to beginning and end of line
  noremap - $
  noremap _ ^

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable "Enable syntax hl

if has("gui_running")
  set guioptions-=T
  set t_Co=256
  set background=dark
 " colorscheme slate
else
 " colorscheme slate
  set background=dark
endif

set encoding=utf8
try
    lang en_US
catch
endtry

set ffs=unix,dos,mac "Default file types

:" Only do this part when compiled with support for autocommands. 
:if has("autocmd") 
:autocmd Filetype java setlocal omnifunc=javacomplete#Complete 
:endif 

:setlocal completefunc=javacomplete#CompleteParamsInfo 

""""""""""""""""""""""""""""""""""""""""""""
"                vim latex                 "
""""""""""""""""""""""""""""""""""""""""""""
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'

""""""""""""""""""""""""""""""""""""""""""""
"              X11 clipboard               "
""""""""""""""""""""""""""""""""""""""""""""
:com -range Cz :silent :<line1>,<line2>w !xsel -i -b
:com -range Cx :silent :<line1>,<line2>w !xsel -i -p
:com -range Cv :silent :<line1>,<line2>w !xsel -i -s
:ca cv Cv
:ca cz Cz
:ca cx Cx

:com -range Pz :silent :r !xsel -o -b
:com -range Px :silent :r !xsel -o -p
:com -range Pv :silent :r !xsel -o -s

:ca pz Pz
:ca px Px
:ca pv Pv

" Copy to X CLIPBOARD
 map <leader>cc :w !xsel -i -b<CR>
 map <leader>cp :w !xsel -i -p<CR>
 map <leader>cs :w !xsel -i -s<CR>
 " Paste from X CLIPBOARD
 map <leader>pp :r!xsel -p<CR>
 map <leader>ps :r!xsel -s<CR>
 map <leader>pb :r!xsel -b<CR>

"set the filename to always show
set modeline
set ls=2

" Store swap files in fixed location, not current directory.
set dir=~/.vim/.vimswap//,.
